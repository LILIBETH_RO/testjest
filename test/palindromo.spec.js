import { Palindromo } from "../src/palindromo.js";

describe('Palindromo', () => {
    it('frase', () => {
        const frasePalindromo = "Yo dono rosas, oro no doy";

        const frase = new Palindromo();

        expect(frase.frase(frasePalindromo));
    })
})